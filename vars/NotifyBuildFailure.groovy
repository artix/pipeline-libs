#!/usr/bin/env groovy

def call(def pkg) {

    if ( pkg.config.global.mail ) {

        emailext (
            mimeType: pkg.config.notify.mime,
            subject: pkg.getEmailSubject("Failure"),
            body: pkg.getEmailBuildBody(BUILD_URL),
            to: pkg.config.notify.fails,
            attachmentsPattern: '**/*build.log **/*check.log',
            compressLog: false,
            attachLog: true
        )

    }

}

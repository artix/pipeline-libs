#!/usr/bin/env groovy

def call(def pkg) {

    catchError(message: "Failed to load config",
        buildResult: 'FAILURE',
        stageResult: 'FAILURE') {

        pkg.loadConfig()
    }

}

#!/usr/bin/env groovy

def call(def pkg) {

    if ( pkg.config.global.mail ) {

        emailext (
            mimeType: pkg.config.notify.mime,
            subject: pkg.getEmailSubject('Success'),
            body: pkg.getEmailRepoBody(),
            to: pkg.config.notify.repos
        )

    }

}

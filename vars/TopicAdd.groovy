#!/usr/bin/env groovy

def call(def pkg) {

    withCredentials([string(credentialsId: 'jenkins-gitea-token', variable: 'TOKEN')]) {

        httpRequest (
            quiet: true,
            httpMode: 'PUT',
            consoleLogResponseBody: false,
            validResponseCodes: '100:399,404',
            customHeaders: [
                [maskValue: false, name: 'accept', value: 'application/json'],
                [maskValue: true, name: 'Authorization', value: "token " + TOKEN]
            ],
            url: pkg.topicUrl.toAdd,
            wrapAsMultipart: false
        )

    }
}


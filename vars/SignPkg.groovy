#!/usr/bin/env groovy

def call(def pkg) {

    catchError(message: "Failed to sign",
        buildResult: 'FAILURE',
        stageResult: 'FAILURE') {

//         withCredentials([
//             usernamePassword(
//                 credentialsId: "${pkg.maintainer.credentialsId}",
//                 passwordVariable: 'GPG_PASS',
//                 usernameVariable: 'GPG_KEY'
//             )
//         ]) {
            withCredentials([
                string(
                    credentialsId: "${pkg.maintainer.credentialsId}",
                    variable: 'BUILDBOT_GPGP'
                )
            ]) {
            if ( pkg.config.global.dryRun ) {

                echo "${pkg.config.tools.cmdSign}"

            } else {

                sh(script: pkg.config.tools.cmdSign,  returnStatus: false)

            }

        }

    }

}

#!/usr/bin/env groovy

def call(def pkg){

    catchError(message: "library *.so changes detected.",
        buildResult: 'SUCCESS',
        stageResult: 'UNSTABLE') {

        if ( pkg.config.global.dryRun ) {

            echo "${pkg.config.tools.cmdCheck}"

        } else {

            byte changed = sh(script: pkg.config.tools.cmdCheck, returnStatus: true)

            if ( changed == 1 ) {

                final String attach = '**/*-checkpkg.log'

                if ( pkg.config.global.mail ) {

                    emailext (
                        mimeType: pkg.config.notify.mime,
                        subject: pkg.getEmailSubject("Unstable check"),
                        body: pkg.getEmailBuildBody(BUILD_URL),
                        to: pkg.config.notify.checks,
                        attachmentsPattern: attach,
                        attachLog: true
                    )

                }

                archiveArtifacts (
                    allowEmptyArchive: true,
                    artifacts: attach
                )
            }

            sh(script: "exit ${changed}", returnStatus: false)
        }

    }

}

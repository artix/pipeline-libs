#!/usr/bin/env groovy

def call(def pkg){

    if ( pkg.meta.actions.triggersRepoAdd ) {
        pkg.config.runStages.check = true
        pkg.config.runStages.sign = true
    }

    archiveArtifacts (
        allowEmptyArchive: true,
        artifacts: '**/*.log'
    )

}

#!/usr/bin/env groovy

def call(def pkg){

    if ( pkg.meta.actions.triggersRepoAdd ) {
        pkg.config.runStages.add = true
    }

}

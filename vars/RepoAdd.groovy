#!/usr/bin/env groovy

def call(def pkg) {

    catchError(message: "Errors occurred.",
        buildResult: 'UNSTABLE',
        stageResult: 'UNSTABLE') {

        if ( pkg.config.global.dryRun ) {

            echo "${pkg.config.tools.cmdRepoAdd}"

        } else {

            sh(script: pkg.config.tools.cmdRepoAdd, returnStatus: false)

        }

    }

}

#!/usr/bin/env groovy

def call(def pkg) {

    LoadConfig(pkg)

    pipeline {
        agent { label pkg.config.agents.master }
        options {
            skipDefaultCheckout()
            timestamps()
            ansiColor('xterm')
        }
        stages {
            stage('Checkout') {
                steps {
                    CheckOut(pkg)
                }
            }
            stage('Build') {
                when {
                    expression { return pkg.config.runStages.build }
                }
                steps {
                    BuildPkg(pkg)
                }
                post {
                    success { RunCheckAndSign(pkg) }
                }
            }
            stage('Check') {
                when {
                    expression { return pkg.config.runStages.check }
                }
                steps {
                    CheckPkg(pkg)
                }
            }
            stage('Sign') {
                when {
                    expression { return pkg.config.runStages.sign }
                }
                steps {
                    SignPkg(pkg)
                }
                post {
                    success { RunAdd(pkg) }
                }
            }
            stage('Add') {
                when {
                    expression { return pkg.config.runStages.add }
                }
                steps {
                    RepoAdd(pkg)
                }
                post {
                    success { TopicAdd(pkg) }
                    unsuccessful { NotifyRepoUnsuccessful(pkg) }
                }
            }
            stage('Remove') {
                when {
                    expression { return pkg.config.runStages.remove }
                }
                steps {
                    RepoRemove(pkg)
                }
                post {
                    success { TopicRemove(pkg) }
                    unsuccessful { NotifyRepoUnsuccessful(pkg) }
                }
            }
        }
        post {
            success { NotifyRepoSuccess(pkg) }
            failure { NotifyBuildFailure(pkg) }
            fixed { NotifyBuildFixed(pkg) }
        }
    }

}

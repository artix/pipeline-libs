#!/usr/bin/env groovy

def call(def pkg){

    catchError(message: "Failed to build source",
        buildResult: 'FAILURE',
        stageResult: 'FAILURE') {

        if ( pkg.config.global.dryRun ) {

            echo "${pkg.config.tools.cmdBuild}"

        } else {

            sh(script: pkg.config.tools.cmdBuild,  returnStatus: false)

        }

    }

}

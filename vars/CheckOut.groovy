#!/usr/bin/env groovy

def call(def pkg) {

    def scm = checkout(scm)
    pkg.initialize(scm)

    currentBuild.displayName = pkg.fullName
    currentBuild.description = pkg.repo

    pkg.meta.actions.each { it -> echo "${it}" }

    pkg.maintainer.each { it -> echo "${it}" }
}

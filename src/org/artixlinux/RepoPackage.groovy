#!/usr/bin/env groovy

package org.artixlinux
class RepoPackage implements Serializable {

    Map config = [:]

    Map meta = [:]

    Map pkgFiles = [toAdd: [], toRemove: []]

    Map maintainer = [name: null, email: null, credentialsId: null]

    Map topicUrl = [toAdd: null, toRemove: null]

    String repo = ''

    String fullName = ''

    String baseName = ''

    def steps

    RepoPackage(steps) {
        this.steps = steps
    }

    String getEmailSubject(String result) {
        String subject = ''
        if ( meta.actions.addRepo ) {
            subject += "[${meta.actions.addRepo}]"
        }
        if ( meta.actions.removeRepo ) {
            subject += " [${meta.actions.removeRepo}]"
        }
        return "${subject} ${result}: ${fullName}"
    }

    String getEmailBuildBody(String url) {
        return """
            <p>Build: ${meta.actions.addRepo}</p>
            <p>Name: ${fullName}</p>
            <p>maintainer: ${maintainer.name}</p>
            <p>email: ${maintainer.email}</p>
            <p><a href=${url}>${url}</a></p>
        """
    }

    String getEmailRepoBody() {
        String body = """
            <p>maintainer: ${maintainer.name}</p>
            <p>email: ${maintainer.email}</p>
        """
        if ( meta.actions.addRepo ) {
            body += """
                <p>Repo: ${meta.actions.addRepo}</p>
                <p>Packages added:</p>
            """ + pkgFiles['toAdd'].collect() { "<p>${it}</p>" }.join('\n')
        }
        if ( meta.actions.removeRepo ) {
            body += """
                <p>Repo: ${meta.actions.removeRepo}</p>
                <p>Packages removed:</p>
            """ + pkgFiles['toRemove'].collect() { "<p>${it}</p>" }.join('\n')
        }
        return body
    }

    void loadConfig() {
        final String conf = steps.libraryResource('org/artixlinux/config.yaml')
        this.config = steps.readYaml(text: conf)
    }

    void initialize(def scm) {

        this.meta = steps.readYaml(file: config.db)

        this.fullName = "${meta.pkgbase.name}-${meta.pkgbase.version}"

        this.baseName = scm.GIT_URL.tokenize('/.')[-2]

        final String apiUrl = "${config.gitea.url}/" + "${config.gitea.org}/" + "${baseName}/topics"

        this.topicUrl.toAdd = "${apiUrl}/${meta.actions.addRepo}"

        this.topicUrl.toRemove = "${apiUrl}/${meta.actions.removeRepo}"

        final String commit = scm.GIT_COMMIT
        String cmdGit = "git show -s --format='%an' ${commit}"
        this.maintainer.name = steps.sh(returnStdout: true, script: cmdGit).trim()

        cmdGit = "git show -s --format='%ae' ${commit}"
        this.maintainer.email = steps.sh(returnStdout: true, script: cmdGit).trim()

        final String packagerCredentials = "BUILDBOT"
        // maintainer.name.toUpperCase()
        this.maintainer.credentialsId = "${packagerCredentials}_GPGP"

        final String packager = "'${maintainer.name} <${maintainer.email}>'"

        if ( meta.actions.triggersBuild ) {
            this.config.runStages.build = true
            this.config.tools.cmdBuild += " -a ${config.arch} -d ${meta.actions.addRepo}"
            this.config.tools.cmdBuild += " -e ${packager}"
            this.config.tools.cmdCheck += " -a ${config.arch} -d ${meta.actions.addRepo}"
        }

        if ( meta.actions.triggersRebuild ) {
            String stable = meta.actions.addRepo.minus('-goblins')
            this.config.runStages.build = true
            this.config.tools.cmdBuild += " -a ${config.arch} -d ${stable} -m"
            this.config.tools.cmdBuild += " -e ${packager}"
            this.config.tools.cmdCheck += " -a ${config.arch} -d ${stable}"
        }

        if ( meta.actions.triggersNoCheck ) {
            this.config.tools.cmdBuild += " -N"
        }

        if ( meta.actions.addRepo ) {

            this.repo = meta.actions.addRepo

            this.pkgFiles.toAdd = meta.repos[repo].packages.collect { it }

            final String addArgs = pkgFiles.toAdd.join(' ')
            this.config.tools.cmdRepoAdd += " -d ${repo} ${addArgs}"

            this.config.tools.cmdSign += " ${addArgs}"

            if ( ! config.runStages.build ) {
                if ( meta.actions.triggersRepoAdd ) {
                    this.config.runStages.add = true
                }
            }
        }

        if ( meta.actions.removeRepo ) {

            this.pkgFiles.toRemove = meta.pkgbase.pkgname.collect { it }

            final String rmArgs = pkgFiles.toRemove.join(' ')

            this.config.tools.cmdRepoRemove += " -d ${meta.actions.removeRepo} ${rmArgs}"

            if ( ! config.runStages.build ) {
                if ( meta.actions.triggersRepoRemove ) {
                    this.config.runStages.remove = true
                }
            }

            if ( ! meta.actions.addRepo ) {
                this.repo = meta.actions.removeRepo
            }
        }

    }

}
